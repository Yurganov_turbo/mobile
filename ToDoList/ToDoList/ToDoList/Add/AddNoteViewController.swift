//
//  AddNoteViewController.swift
//  ToDoList
//
//  Created by Dmitry Yurganov on 04.06.2020.
//  Copyright © 2020 Dmitry Yurganov. All rights reserved.
//

import UIKit

class AddNoteViewController: UIViewController {
    @IBOutlet private weak var titleTextField: UITextField!
    @IBOutlet private weak var noteTextField: UITextField!
    @IBOutlet private weak var alertView: UIView!
    
    weak var delegate: AddNoteDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        titleTextField.delegate = self
        noteTextField.delegate = self
        
        alertView.layer.borderWidth = 1
        alertView.layer.borderColor = UIColor.white.cgColor
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
         NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    

    @IBAction func didTapAdd(_ sender: Any) {
        guard let title = titleTextField.text, let note = noteTextField.text else {
            return
        }
        
        delegate?.didTapAddNewNote(self, titleText: title, noteText: note)
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func didTapCancel(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height - (UIScreen.main.bounds.height - alertView.frame.maxY)
            }
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
}

extension AddNoteViewController: UITextFieldDelegate {
//    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//        if textField == noteTextField {
//            let alertOrigin = CGPoint(x: alertView.frame.origin.x, y: alertView.frame.origin.y -
//            alertView.frame = CGRect(origin: <#T##CGPoint#>, size: alertView.bounds.size)
//        }
//
//        return true
//    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if textField == titleTextField { // Switch focus to other text field
            noteTextField.becomeFirstResponder()
        }
        
        if textField == noteTextField {
            didTapAdd(textField)
        }
        
        return true
    }
}

protocol AddNoteDelegate: class {
    func didTapAddNewNote(_ from: AddNoteViewController, titleText: String, noteText: String)
}
