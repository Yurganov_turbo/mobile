//
//  ToDoListViewModel.swift
//  ToDoList
//
//  Created by Dmitry Yurganov on 13.05.2020.
//  Copyright © 2020 Dmitry Yurganov. All rights reserved.
//

import Foundation

class ToDoListViewModel {
    private var notesList: [Note] = []
    private(set) var sections: [String] = ["Finished", "Not Finished"]
    var delegate: ToDoListViewModelVisualDelegate?
    
    init() {
        if let notes = LocalStorageManager.loadNotebook() {
            notesList = notes
        }
    }
    
    func notesCount() -> Int {
        notesList.count
    }
    
    func notFinishedNotes() -> [Note] {
        notesList.filter{ !$0.isFinish }
    }
    
    func finishedNotes() -> [Note] {
        notesList.filter { $0.isFinish }
    }
    
    func finishNote(index: Int) {
        guard index < notesList.count else { return }
        notesList[index].isFinish = !notesList[index].isFinish
        LocalStorageManager.saveNotebook(notebook: notesList)
    }
}

extension ToDoListViewModel: AddNoteDelegate {
    func didTapAddNewNote(_ from: AddNoteViewController, titleText: String, noteText: String) {
        let note = Note(title: titleText, text: noteText, isFinish: false)
        notesList.append(note)
        LocalStorageManager.saveNotebook(notebook: notesList)
        delegate?.updateTable(self)
    }
}


protocol ToDoListViewModelVisualDelegate: class {
    func updateTable(_ viewModel: ToDoListViewModel)
}
