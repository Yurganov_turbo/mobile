//
//  Note.swift
//  ToDoList
//
//  Created by Dmitry Yurganov on 13.05.2020.
//  Copyright © 2020 Dmitry Yurganov. All rights reserved.
//

import Foundation

class Note: NSObject , NSCoding {
    var title: String {
        didSet {
            updateDate()
        }
    }
    
    var text: String {
        didSet {
            updateDate()
        }
    }
    
    var date: Date = Date()
    var isFinish:  Bool
    
    
    func encode(with coder: NSCoder) {
        coder.encode(self.title,  forKey: "title");
        coder.encode(self.text,    forKey: "text");
        coder.encode(self.date,    forKey: "date");
        coder.encode(self.isFinish, forKey: "isFinish")
    }
    
    required init?(coder: NSCoder) {
      //  super.init()
        
        self.title = coder.decodeObject(forKey: "title") as! String
        self.text = coder.decodeObject(forKey: "text") as! String
        self.date = coder.decodeObject(forKey: "date") as! Date
        self.isFinish = coder.decodeBool(forKey: "isFinish")
    }
    
    init(title: String, text: String, isFinish: Bool){
        self.title = title
        self.text = text
        self.isFinish = isFinish
    }
    
    private func updateDate() {
        date = Date()
    }
}
