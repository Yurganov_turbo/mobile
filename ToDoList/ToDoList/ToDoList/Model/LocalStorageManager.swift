//
//  LocalStorageManager.swift
//  ToDoList
//
//  Created by Dmitry Yurganov on 05.06.2020.
//  Copyright © 2020 Dmitry Yurganov. All rights reserved.
//

import Foundation

class LocalStorageManager {
    class func saveNotebook(notebook: [Note]) {
        let fileName = "notebook"
        let fullPath = getDocumentsDirectory().appendingPathComponent(fileName)

        do {
            let data = try NSKeyedArchiver.archivedData(withRootObject: notebook, requiringSecureCoding: false)
            try data.write(to: fullPath)
        } catch {
            print("Couldn't write file")
        }
    }
    
    class func loadNotebook() -> [Note]? {
        let fileName = "notebook"
        let fullPath = getDocumentsDirectory().appendingPathComponent(fileName)
        guard let data = try? Data(contentsOf: fullPath) else { return nil }
        do {
            if let loadedNotebook = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as? [Note] {
                return loadedNotebook
            }
        } catch {
            print("Couldn't read file.")
        }
        
        return nil
    }
    
    private class func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
}
