//
//  ToDoListViewController.swift
//  ToDoList
//
//  Created by Dmitry Yurganov on 13.05.2020.
//  Copyright © 2020 Dmitry Yurganov. All rights reserved.
//

import UIKit

class ToDoListViewController: UIViewController {

    @IBOutlet private weak var toDoListTableView: UITableView!
    var viewModel: ToDoListViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupAddButton()
    }
    
    private func setupAddButton() {
        navigationController?.navigationBar.topItem?.title = "To do list"
        
        overrideUserInterfaceStyle = .dark

        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(didTapAddButton))
    }
    
    @objc func didTapAddButton() {
        let addNewNoteAlert = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddNoteViewController") as! AddNoteViewController
        addNewNoteAlert.modalPresentationStyle = .overFullScreen
        addNewNoteAlert.delegate = viewModel
        self.present(addNewNoteAlert, animated: false)
    }

}

extension ToDoListViewController: UITableViewDelegate, UITableViewDataSource, NoteTableViewCellViewModelDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.sections.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return viewModel.notFinishedNotes().count
        default:
            return viewModel.finishedNotes().count
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.isSelected = false
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NoteTableViewCell", for: indexPath) as! NoteTableViewCell
        switch indexPath.section {
        case 0:
            let note = viewModel.notFinishedNotes()[indexPath.row]
            let cellViewModel = NoteTableViewCellViewModel(title: note.title, text: note.text, date: note.date, isFinished: note.isFinish)
            cellViewModel.delegate = self
            cell.viewModel = cellViewModel
        default:
            let note = viewModel.finishedNotes()[indexPath.row]
            let cellViewModel = NoteTableViewCellViewModel(title: note.title, text: note.text, date: note.date, isFinished: note.isFinish)
            cellViewModel.delegate = self
            cell.viewModel = cellViewModel
        }
    
        return cell
    }
    
    func didChangeFinishState(_ cell: NoteTableViewCell) {
        if let index = toDoListTableView.indexPath(for: cell) {
            viewModel.finishNote(index: index.row)
        }
    }
}

extension ToDoListViewController: ToDoListViewModelVisualDelegate {
    func updateTable(_ viewModel: ToDoListViewModel) {
        self.toDoListTableView.reloadData()
    }
}
