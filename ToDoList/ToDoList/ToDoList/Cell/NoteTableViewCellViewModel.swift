//
//  NoteTableViewCellViewModel.swift
//  ToDoList
//
//  Created by Dmitry Yurganov on 13.05.2020.
//  Copyright © 2020 Dmitry Yurganov. All rights reserved.
//

import Foundation

class NoteTableViewCellViewModel {
    let title: String
    let text: String
    let date: Date
    var isFinished: Bool
    
    var delegate: NoteTableViewCellViewModelDelegate?
    
    init(title: String, text: String, date: Date, isFinished: Bool) {
        self.title = title
        self.text = text
        self.date = date
        self.isFinished = isFinished
    }
    
    func dateString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let dateString = dateFormatter.string(from: date)
        return dateString
    }
    
    func didSwitch(cell: NoteTableViewCell) {
        delegate?.didChangeFinishState(cell)
    }
    
}

protocol NoteTableViewCellViewModelDelegate: class {
    func didChangeFinishState(_ cell: NoteTableViewCell)
}
