//
//  NoteTableViewCell.swift
//  ToDoList
//
//  Created by Dmitry Yurganov on 13.05.2020.
//  Copyright © 2020 Dmitry Yurganov. All rights reserved.
//

import UIKit

class NoteTableViewCell: UITableViewCell {
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var noteTextLabel: UILabel!
    @IBOutlet private weak var dateLabel: UILabel!
    @IBOutlet private weak var isFinishSwitch: UISwitch!
    
    var viewModel: NoteTableViewCellViewModel! {
        didSet {
            titleLabel.text = viewModel.title
            noteTextLabel.text = viewModel.text
            dateLabel.text = viewModel.dateString()
            isFinishSwitch.isOn = viewModel.isFinished
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func didChangeValueSwitch(_ sender: Any) {
        viewModel.didSwitch(cell: self)
    }
}
