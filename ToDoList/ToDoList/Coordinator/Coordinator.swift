//
//  Coordinator.swift
//  ToDoList
//
//  Created by Dmitry Yurganov on 13.05.2020.
//  Copyright © 2020 Dmitry Yurganov. All rights reserved.
//

import UIKit

protocol Coordinator {
    var rootViewController: UIViewController { get }
    
    func start()
}

class AppCoordinator: Coordinator {
    var rootViewController: UIViewController {
        return navigationController
    }

    private let navigationController: UINavigationController = UINavigationController()
    
    func start() {
        guard let noteViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ToDoListViewController") as? ToDoListViewController else {
            return
        }
        
        let viewModel = ToDoListViewModel()
        viewModel.delegate = noteViewController
        noteViewController.viewModel = viewModel
        navigationController.pushViewController(noteViewController, animated: true)
    }
    

}
